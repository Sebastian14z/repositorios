<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/repositorios/app/core/BaseController.php');

class historial_repositorio extends BaseController
{
    /**
     * @throws SmartyException
     */
    function __construct()
    {
        parent::__construct();
        $sql = "select r.nombre, u.nombre as `usuario`, hr.accion, hr.fecha, hr.repositorio as `contenido`
                from $this->base.historial_repositorio hr
                    inner join $this->base.repositorio r on hr.cod_repositorio = r.cod_repositorio
                    inner join $this->base.usuario u on hr.cod_usuario = u.cod_usuario
                order by hr.fecha desc";
        $historial = $this->conexion->consultar($sql);
        $this->asignarVariableVista([
            'habilita_navbar' => true,
            'habilita_footer' => true,
            'tab_activa' => 'historial',
            'historial' => $historial
        ]);
        $this->renderizarVista('tablaHistorial.tpl');
    }
}

new historial_repositorio();