<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/repositorios/app/core/BaseController.php');

class repositorio extends BaseController
{
    /**
     * @throws SmartyException
     */
    function __construct()
    {
        parent::__construct();
        if ($_POST) {
            switch ($_POST['funcion']) {
                case 'crea-actualiza':
                    $this->creaActualizaRepositorio($_POST);
                    die;
                default:
                    $this->renderizarVista('404.tpl');
                    die;
            }
        } elseif ($_GET) {
            switch ($_GET['funcion']) {
                case 'crear-actualiza':
                    $cod_repositorio = (isset($_GET['repositorio'])) ? $_GET['repositorio'] : 0;
                    $this->formCreaActualizaRepositorio($cod_repositorio);
                    die;
                case 'eliminar':
                    $cod_repositorio = $_GET['repositorio'];
                    $this->eliminarRepositorio($cod_repositorio);
                    die;
                case 'consultar':
                    $cod_repositorio = $_GET['repositorio'];
                    $this->consultarRepositorio($cod_repositorio);
                    die;
                default:
                    $this->renderizarVista('404.tpl');
                    die;
            }
        }
        $this->vistaPrincipal();
    }

    /**
     * @throws SmartyException
     */
    private function vistaPrincipal()
    {
        $repositorios = $this->conexion->consultar(
            "select r.*, u.nombre as `usuario`
             from $this->base.repositorio r
                 inner join $this->base.usuario u on r.cod_usuario = u.cod_usuario"
        );
        $this->asignarVariableVista([
            'habilita_navbar' => true,
            'habilita_footer' => true,
            'tab_activa' => 'repositorios',
            'repositorios' => $repositorios
        ]);
        $this->renderizarVista('tablaRepositorio.tpl');
        http_response_code(200);
    }

    /**
     * @param int $cod_repositorio
     * @throws SmartyException
     */
    private function formCreaActualizaRepositorio($cod_repositorio = 0)
    {
        $base = $this->base;
        $this->asignarVariableVista([
            'habilita_navbar' => true,
            'habilita_footer' => true,
            'tab_activa' => 'repositorios'
        ]);
        if ($cod_repositorio) {
            $repositorio = $this->conexion->consultar(
                "select r.*, u.nombre as `usuario`
                 from $base.repositorio r
                     inner join $base.usuario u on r.cod_usuario = u.cod_usuario
                 where r.cod_repositorio = $cod_repositorio"
            )[0];
            $this->asignarVariableVista(['repositorio' => $repositorio]);
            $this->renderizarVista('actualizaRepositorio.tpl');
        } else {
            $this->renderizarVista('creaRepositorio.tpl');
        }
    }

    /**
     * @param $data
     */
    private function creaActualizaRepositorio($data)
    {
        $base = $this->base;
        if (isset($data['cod_repositorio'])) {
            $cod_repositorio = $data['cod_repositorio'];
            $nombre = "`nombre`='" . $data['nombre'] . "'";
            $contenido = "`contenido`='" . $data['contenido'] . "'";
            $usuario_modifica = "`cod_usuario_modifica`=" . $data['cod_usuario'];
            $sql = "update $base.repositorio set $nombre, $usuario_modifica, $contenido
                    where cod_repositorio=$cod_repositorio";
            $response = $this->conexion->actualizaElimina($sql);
        } else {
            $nombre = "'" . $data['nombre'] . "'";
            $contenido = "'" . $data['contenido'] . "'";
            $usuario = $data['cod_usuario'];
            $usuario_modifica = $data['cod_usuario'];
            $sql = "insert into $base.repositorio (`nombre`, `contenido`, `cod_usuario`, `cod_usuario_modifica`) values
                        ($nombre, $contenido, $usuario, $usuario_modifica)";
            $response = $this->conexion->insertar($sql);
        }
        die($response);
    }

    /**
     * @param $cod_repositorio
     */
    private function eliminarRepositorio($cod_repositorio)
    {
        $base = $this->base;
        $this->conexion->actualizaElimina("delete from $base.repositorio where cod_repositorio=$cod_repositorio");
    }

    /**
     * @throws SmartyException
     */
    private function consultarRepositorio($cod_repositorio)
    {
        $this->insertarConsultaRepositorio($cod_repositorio);

        $sql = "select r.*, u.nombre as `usuario`
                from $this->base.repositorio r inner join $this->base.usuario u on r.cod_usuario = u.cod_usuario
                where r.cod_repositorio = $cod_repositorio";
        $repositorio = $this->conexion->consultar($sql)[0];
        $this->asignarVariableVista([
            'habilita_navbar' => true,
            'habilita_footer' => true,
            'tab_activa' => 'repositorios',
            'repositorio' => $repositorio
        ]);
        $this->renderizarVista('consultarContenido.tpl');
        http_response_code(200);
    }

    private function insertarConsultaRepositorio($cod_repositorio)
    {
        $cod_usuario = 1; # TODO: USUARIO DE LA SESION
        $sql = "insert into $this->base.historial_repositorio (cod_repositorio, cod_usuario, accion) values 
                ($cod_repositorio, $cod_usuario, 'Consultar')";
        $this->conexion->insertar($sql);
    }
}

new repositorio();