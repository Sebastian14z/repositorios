<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/repositorios/app/core/BaseController.php');

class home extends BaseController
{
    /**
     * @throws SmartyException
     */
    function __construct()
    {
        parent::__construct();
        $this->asignarVariableVista([
            'habilita_navbar' => true,
            'habilita_footer' => true,
            'tab_activa' => 'home'
        ]);
        $this->renderizarVista('home.tpl');
    }
}

new home();