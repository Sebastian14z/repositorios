<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/repositorios/app/core/BaseController.php');

class usuario extends BaseController
{
    /**
     * @throws SmartyException
     */
    function __construct()
    {
        parent::__construct();
        if ($_POST) {
            switch ($_POST['funcion']) {
                case 'crea-actualiza':
                    $this->creaActualizaUsuario($_POST);
                    die;
                default:
                    $this->renderizarVista('404.tpl');
                    die;
            }
        } elseif ($_GET) {
            switch ($_GET['funcion']) {
                case 'crear-actualiza':
                    $cod_usuario = (isset($_GET['usuario'])) ? $_GET['usuario'] : 0;
                    $this->formCreaActualizaUsuario($cod_usuario);
                    die;
                case 'eliminar':
                    $cod_usuario = $_GET['usuario'];
                    $this->eliminarUsuario($cod_usuario);
                    die;
                default:
                    $this->renderizarVista('404.tpl');
                    die;
            }
        }
        $this->vistaPrincipal();
    }

    /**
     * @throws SmartyException
     */
    private function vistaPrincipal()
    {
        $usuarios = $this->conexion->consultar(
            "select u.*, p.nombre as `perfil`
                   from $this->base.usuario u 
                       inner join $this->base.perfil p on u.cod_perfil = p.cod_perfil"
        );
        $this->asignarVariableVista([
            'habilita_navbar' => true,
            'habilita_footer' => true,
            'tab_activa' => 'usuarios',
            'usuarios' => $usuarios
        ]);
        $this->renderizarVista('tablaUsuarios.tpl');
        http_response_code(200);
    }

    /**
     * @param $cod_usuario
     * @throws SmartyException
     */
    private function formCreaActualizaUsuario($cod_usuario = 0)
    {
        $base = $this->base;
        $variables_vista = [];
        $variables_vista['perfiles'] = $this->conexion->consultar("select cod_perfil, nombre from $base.perfil");
        $variables_vista['habilita_navbar'] = true;
        $variables_vista['habilita_footer'] = true;
        $variables_vista['tab_activa'] = 'usuarios';
        if ($cod_usuario) {
            $variables_vista['usuario'] = $this->conexion->consultar(
                "select u.*,
                              p.nombre as `perfil`
                        from $base.usuario u inner join $base.perfil p on u.cod_perfil = p.cod_perfil
                        where cod_usuario = $cod_usuario"
            )[0];
            $this->asignarVariableVista($variables_vista);
            $this->renderizarVista('actualizaUsuario.tpl');
        } else {
            $this->asignarVariableVista($variables_vista);
            $this->renderizarVista('creaUsuario.tpl');
        }
    }

    /**
     * @param $data
     */
    private function creaActualizaUsuario($data)
    {
        $base = $this->base;
        if (isset($data['cod_usuario'])) {
            $cod_usuario = $data['cod_usuario'];
            $sql = "select contrasena from $this->base.usuario where cod_usuario = $cod_usuario";
            $usuario = $this->conexion->consultar($sql)[0];
            $contrasena_encriptada = $this->conexion->encriptar($data['contrasena']);
            if ($usuario['contrasena'] == $contrasena_encriptada) {
                $contrasena = "`contrasena`='" . $data['contrasena'] . "'";
            } else {
                $contrasena = "`contrasena`='" . $contrasena_encriptada . "'";
            }
            $nombre = "`nombre`='" . $data['nombre'] . "'";
            $correo = "`correo`='" . $data['correo'] . "'";
            $perfil = "`cod_perfil`=" . $data['cod_perfil'];
            $sql = "update $base.usuario set $nombre, $correo, $contrasena, $perfil where cod_usuario=$cod_usuario";
            $response = $this->conexion->actualizaElimina($sql);
        } else {
            $nombre = "'" . $data['nombre'] . "'";
            $correo = "'" . $data['correo'] . "'";
            $contrasena = "'" . $this->conexion->encriptar($data['contrasena']) . "'";
            $perfil = $data['cod_perfil'];
            $sql = "insert into $base.usuario (`nombre`, `correo`, `contrasena`, `cod_perfil`) values
                        ($nombre, $correo, $contrasena, $perfil)";
            print($sql);
            $response = $this->conexion->insertar($sql);
        }
        die($response);
    }

    /**
     * @param $cod_usuario
     */
    private function eliminarUsuario($cod_usuario)
    {
        $base = $this->base;
        $this->conexion->actualizaElimina("delete from $base.usuario where cod_usuario=$cod_usuario");
    }
}

new usuario();