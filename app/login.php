<?php
include($_SERVER['DOCUMENT_ROOT'] . '/repositorios/libs/smarty/libs/Smarty.class.php');
include($_SERVER['DOCUMENT_ROOT'] . '/repositorios/app/conexion/conexion.php');
new login();

class login
{
    private $conexion, $base, $ruta_base, $smarty;

    /**
     * @throws SmartyException
     */
    function __construct()
    {

        $this->conexion = new conexion();
        $this->base = 'id17939146_repositorios';
        $this->ruta_base = $_SERVER['DOCUMENT_ROOT'] . '/repositorios';

        $this->smarty = new Smarty;
        $this->smarty->debugging = false;
        $this->smarty->caching = false;
        $this->smarty->cache_lifetime = 10;
        $this->smarty->template_dir = "$this->ruta_base/app/views";
        $this->smarty->compile_dir = 'cache';

        if ($_POST) {
            $this->validarIngreso($_POST);
        } elseif ($_GET) {
            if ($_GET['funcion'] == 'cerrar-sesion') {
                $this->cerrarSesion();
            }
        }
        $this->renderizarLogin();
    }

    /**
     * @throws SmartyException
     */
    private function renderizarLogin()
    {
        $this->smarty->assign('credenciales_incorrectas', false);
        $this->smarty->assign('habilita_navbar', false);
        $this->smarty->assign('habilita_footer', false);
        $this->smarty->display('login.tpl');
    }

    private function validarIngreso($data)
    {
        $this->limpiarCookies();

        $email = $data['email'];
        $contrasena = $this->conexion->encriptar($data['contrasena']);

        $sql = "select u.*, p.nombre as `perfil` from $this->base.usuario u
                    inner join $this->base.perfil p on u.cod_perfil = p.cod_perfil
                where correo='$email' and contrasena = '$contrasena'";
        $usuario = $this->conexion->consultar($sql);
        if (!empty($usuario)) {
            setcookie("cod_usuario", $usuario[0]['cod_usuario'], time() + 3600);
            setcookie("nombre", $usuario[0]['nombre'], time() + 3600);
            setcookie("perfil", $usuario[0]['perfil'], time() + 3600);
            header('Location:home.php');
            die;
        } else {
            $this->smarty->assign('credenciales_incorrectas', true);
            $this->smarty->display('login.tpl');
            die;
        }
    }

    private function cerrarSesion()
    {
        $this->limpiarCookies();
        echo '{"resultado": true}';
        die;
    }

    private function limpiarCookies()
    {
        unset($_COOKIE['cod_usuario']);
        unset($_COOKIE['nombre']);
        unset($_COOKIE['perfil']);
        setcookie('cod_usuario', null, -1, '/');
        setcookie('nombre', null, -1, '/');
        setcookie('perfil', null, -1, '/');
    }
}