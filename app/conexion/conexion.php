<?php

class conexion
{
    private $server;
    private $user;
    private $password;
    private $database;
    private $port;
    private $conexion;

    function __construct()
    {
        $lista_datos = $this->datosConexion();

        foreach ($lista_datos as $value) {
            $this->server = $value['server'];
            $this->user = $value['user'];
            $this->password = $value['password'];
            $this->database = $value['database'];
            $this->port = $value['port'];
        }

        $this->conexion = new mysqli($this->server, $this->user, $this->password, $this->database, $this->port);
        if ($this->conexion->connect_errno) {
            die('ERROR: ' . $this->conexion->connect_errno . ', ' . $this->conexion->connect_error);
        }
    }

    /**
     * @return mixed
     */
    private function datosConexion()
    {
        $path = dirname(__FILE__);
        $data = file_get_contents($path . "/" . "config");
        return json_decode($data, true);
    }

    /**
     * @param $array
     * @return mixed
     */
    private function convertirUTF8($array)
    {
        array_walk_recursive($array, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });
        return $array;
    }

    /**
     * @param $sql_str
     * @return mixed
     */
    public function consultar($sql_str)
    {
        $result_array = [];
        $results = $this->conexion->query($sql_str);
        foreach ($results as $key) {
            $result_array[] = $key;
        }
        return $this->convertirUTF8($result_array);
    }

    /**
     * @param $sql_str
     * @return int
     */
    public function actualizaElimina($sql_str)
    {
        $this->conexion->query($sql_str);
        return $this->conexion->affected_rows;
    }

    /**
     * @param $sql_str
     * @return int|string
     */
    public function insertar($sql_str)
    {
        $this->conexion->query($sql_str);
        $filas = $this->conexion->affected_rows;
        if ($filas >= 1) {
            return $this->conexion->insert_id;
        } else {
            return 0;
        }
    }

    /**
     * @param $string
     * @return string
     */
    public function encriptar($string)
    {
        return md5($string);
    }
}