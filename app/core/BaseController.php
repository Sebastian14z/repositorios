<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: POST, DELETE, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

ini_set('display_errors', 0);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Bogota');

include($_SERVER['DOCUMENT_ROOT'] . '/repositorios/libs/smarty/libs/Smarty.class.php');
include($_SERVER['DOCUMENT_ROOT'] . '/repositorios/app/conexion/conexion.php');

class BaseController
{
    private $smarty;
    protected $conexion, $ruta_base, $base;

    function __construct()
    {
        $this->ruta_base = $_SERVER['DOCUMENT_ROOT'] . '/repositorios';
        $this->conexion = new conexion();
        $this->base = 'id17939146_repositorios';

        $this->smarty = new Smarty;
        $this->smarty->debugging = false;
        $this->smarty->caching = false;
        $this->smarty->template_dir = "$this->ruta_base/app/views";

        $this->smarty->assign('DOCUMENT_ROOT', $this->ruta_base);
        $this->smarty->assign('MAIN_CSS', "$this->ruta_base/public/css/main.css");

        if ($_COOKIE['cod_usuario'] == '') {
            $url = ($_SERVER['REQUEST_URI'] == "/repositorios/app/home.php") ? "login.php" : "../login.php";
            header("Location:$url");
        } else {
            setcookie("cod_usuario", $_COOKIE['cod_usuario'], time() + 300);
            setcookie("nombre", $_COOKIE['nombre'], time() + 300);
            setcookie("perfil", $_COOKIE['perfil'], time() + 300);
            $this->permisosUsuario();
        }
    }

    /**
     * @param $variables
     */
    protected function asignarVariableVista($variables)
    {
        foreach ($variables as $variable => $valor) {
            $this->smarty->assign($variable, $valor);
        }
    }

    /**
     * @param $vista
     * @throws SmartyException
     */
    protected function renderizarVista($vista)
    {
        $this->smarty->display($vista);
    }

    private function permisosUsuario()
    {
        $estudiante = false;
        $cod_usuario = $_COOKIE['cod_usuario'];

        $sql = "select p.permisos from usuario u
                    inner join $this->base.perfil p on u.cod_perfil = p.cod_perfil
                where u.cod_usuario = $cod_usuario";
        $permisos = $this->conexion->consultar($sql)[0]['permisos'];
        $permisos = json_decode($permisos);
        if ($_COOKIE['perfil'] == 'Estudiante') {
            $estudiante = true;
        }
        $this->asignarVariableVista([
            'permisos' => $permisos,
            'nombre_usuario' => $_COOKIE['nombre'],
            'perfil' => $_COOKIE['perfil'],
            'estudiante' => $estudiante
        ]);
    }
}