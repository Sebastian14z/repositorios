CREATE TABLE `id17939146_repositorios`.`usuario`
(
    `cod_usuario` INT          NOT NULL AUTO_INCREMENT,
    `nombre`      VARCHAR(255) NOT NULL,
    `correo`      VARCHAR(255) NOT NULL,
    `constraseña` VARCHAR(255) NOT NULL,
    `cod_perfil`  INT          NOT NULL,
    PRIMARY KEY (`cod_usuario`)
) ENGINE = InnoDB;

INSERT INTO `id17939146_repositorios`.`usuario` (`cod_usuario`, `nombre`, `correo`, `constraseña`, `cod_perfil`)
VALUES (NULL, 'Sebastian Masmela', 'elsebatian14@outlook.es', 'skat14seba', '1');

CREATE TABLE `id17939146_repositorios`.`perfil`
(
    `cod_perfil` INT          NOT NULL AUTO_INCREMENT,
    `nombre`     VARCHAR(255) NOT NULL,
    `permisos`   JSON         NOT NULL,
    PRIMARY KEY (`cod_perfil`)
) ENGINE = InnoDB;

INSERT INTO `id17939146_repositorios`.perfil (nombre, permisos)
VALUES ('Administrador', '[
  "usuarios",
  "repositorios",
  "historial"
]');

ALTER TABLE `id17939146_repositorios`.usuario
    ADD CONSTRAINT FK_usuario_perfil
        FOREIGN KEY (cod_perfil) REFERENCES `id17939146_repositorios`.perfil (cod_perfil);

CREATE TABLE `id17939146_repositorios`.`repositorio`
(
    `cod_repositorio` INT          NOT NULL AUTO_INCREMENT,
    `nombre`          VARCHAR(255) NOT NULL,
    `cod_usuario`     INT          NOT NULL,
    `fecha`           TIMESTAMP    NULL DEFAULT CURRENT_TIMESTAMP,
    `contenido`       LONGTEXT     NOT NULL,
    PRIMARY KEY (`cod_repositorio`)
) ENGINE = InnoDB;

ALTER TABLE `id17939146_repositorios`.repositorio
    ADD CONSTRAINT FK_repositorio_usuario
        FOREIGN KEY (cod_usuario) REFERENCES `id17939146_repositorios`.usuario (cod_usuario);

create table historial_repositorio
(
    cod_historial_repositorio int auto_increment
        primary key,
    cod_repositorio           int                                                   null,
    cod_usuario               int                                                   not null,
    accion                    enum ('Crear', 'Consultar', 'Actualizar', 'Eliminar') not null,
    fecha                     timestamp default current_timestamp()                 not null,
    repositorio               longtext                                              null
);

use id17939146_repositorios;
create trigger eliminacion_repositorio_historial
    after delete
    on repositorio
    for each row
BEGIN
    insert into historial_repositorio (cod_usuario, accion, repositorio)
    values (old.cod_usuario, 'Eliminar', concat(old.nombre, ' | ', old.contenido));
END;
create trigger actualizacion_repositorio_historial
    after update
    on repositorio
    for each row
BEGIN
    insert into historial_repositorio (cod_repositorio, cod_usuario, accion, repositorio)
    values (old.cod_repositorio, old.cod_usuario, 'Actualizar', concat(old.nombre, ' | ', old.contenido));
END;
create trigger creacion_repositorio_historial
    after insert
    on repositorio
    for each row
BEGIN
    insert into historial_repositorio (cod_repositorio, cod_usuario, accion)
    values (new.cod_repositorio, new.cod_usuario, 'Crear');
END;

