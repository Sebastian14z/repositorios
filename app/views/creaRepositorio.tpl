{extends file = "index.tpl"}
{block name=title}Repositorios{/block}
{block name=body}
    <script>
        function crear() {
            const usuario = localStorage.getItem('usuario');
            let contenido = tinymce.get('contenido').getContent();
            let form = {
                funcion: $('#funcion').val(),
                nombre: $('#nombre').val(),
                contenido: contenido,
                cod_usuario: usuario
            }
            $.ajax({
                url: "/repositorios/app/repositorios/repositorio.php?funcion=crea-actualiza",
                method: 'post',
                data: form,
                success: function (result) {
                    swal({
                        title: "Crear",
                        text: "Registro creado correctamente",
                        icon: "success"
                    }).then((value) => {
                        location.href =
                            'http://localhost/repositorios/app/repositorios/repositorio.php';
                    });
                }
            });
        }

        tinymce.PluginManager.add('plantillaRapida', function (editor, url) {
            var plantillaRapida = function () {
                editor.insertContent(
                    '<h1 style="text-align: center;"> Capacitacion Angular </h1>' +
                    '<p>Angular es un framework opensource desarrollado por Google para facilitar la creación y programación de aplicaciones web de una sola página, las webs SPA (Single Page Application).</P>' +
                    '<p> Angular separa completamente el frontend y el backend en la aplicación, evita escribir código repetitivo y mantiene todo más ordenado gracias a su patrón MVC (Modelo-Vista-Controlador) asegurando los desarrollos con rapidez, a la vez que posibilita modificaciones y actualizaciones. </p>' +
                    '<a href="https://angular.io/"> mas informacion </a>'
                );
            }
            editor.ui.registry.addButton('plantillaRapida', {
                text: 'Plantilla Rapida',
                onAction: function () {
                    plantillaRapida();
                }
            });
            return {
                getMetadata: function () {
                    return {
                        name: 'Example plugin',
                        url: 'http://exampleplugindocsurl.com'
                    };
                }
            };
        });
        tinymce.PluginManager.add('DerechosAutor', function (editor, url) {
            var plantillaRapida = function () {
                editor.insertContent(
                    '<p style="border: solid 1px; font-size: 80%; padding: 2%"> Todos los derechos reservados para todos los países, de todos los textos, así como todas las ilustraciones, fotografías y documentos presentados en este sitio. Los textos, ilustraciones, fotografías y documentos son propiedad de sus respectivos autores. ' +
                    ' Cualquier uso para cualquier propósito, textos, documentos, fotografías e información presentada, incluso si es sólo para el uso de una parte o fragmento de esta información no se puede hacer con la previa autorización por escrito del autor de los documentos en cuestión y del propietario del copyright o cualquier otra persona debidamente autorizada a tal efecto. ' +
                    'Se considera un uso: el archivo o transmisión o reproducción de esta información o cualquier parte de la información, en cualquier forma, por ejemplo, en forma impresa, de forma virtual, como un archivo (s) computadora (s), incluyendo en Internet, en la radio, en papel o en cualquier medio, o cualquier otro uso de la información presentada. </p>'
                );
            }
            editor.ui.registry.addButton('DerechosAutor', {
                text: 'Derechos de Autor',
                onAction: function () {
                    plantillaRapida();
                }
            });
            return {
                getMetadata: function () {
                    return {
                        name: 'Example plugin',
                        url: 'http://exampleplugindocsurl.com'
                    };
                }
            };
        });
        tinymce.PluginManager.add('DominioPublico', function (editor, url) {
            var plantillaRapida = function () {
                editor.insertContent(
                    '<p style="border: solid 1px; font-size:80%; padding: 2%"> Todos los contenidos de este espacio web (incluyendo texto, fotografías, archivos de sonido y cualquier otro trabajo original),' +
                    'excepto los indicados están licenciados bajo una licencia ' +
                    'Creative Commons </p>'
                );
            }
            editor.ui.registry.addButton('DominioPublico', {
                text: 'Dominio Publico',
                onAction: function () {
                    plantillaRapida();
                }
            });
            return {
                getMetadata: function () {
                    return {
                        name: 'Example plugin',
                        url: 'http://exampleplugindocsurl.com'
                    };
                }
            };
        });
        tinymce.init({
            selector: 'textarea#contenido',
            plugins: 'plantillaRapida DerechosAutor DominioPublico help',
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | plantillaRapida |' +
                'help | DerechosAutor | DominioPublico',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>
    <h1 class="text-center">
        Crear Recurso
    </h1>
    {*    <textarea id="custom-plugin"></textarea>*}
    <div class="container card">
        <form class="card-body" autocomplete="off" action="#">
            <input type="hidden" id="funcion" value="crea-actualiza">
            <div class="mb-3">
                <label class="form-label">Nombre<req>*</req></label>
                <input type="text" class="form-control" id="nombre" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Contenido<req>*</req></label>
                <textarea class="form-control" placeholder="Realice la descripcion del contenido"
                          id="contenido" required></textarea>
            </div>
            <div class="d-grid gap-2 col-6 mx-auto">
                <button type="button" class="btn btn-primary" onclick="crear()">Guardar</button>
            </div>
        </form>
    </div>
{/block}