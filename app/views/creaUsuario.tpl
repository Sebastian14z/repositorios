{extends file = "index.tpl"}
{block name=title}Crear usuario{/block}
{block name=body}
    <script>
            function crear() {
                let form = {
                    funcion: $('#funcion').val(),
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    contrasena: $('#contrasena').val(),
                    cod_perfil: $('#cod_perfil').val()
                }
                $.ajax({
                    url: "/repositorios/app/usuarios/usuario.php?funcion=crea-actualiza",
                    method: 'post',
                    data: form,
                    success: function(result) {
                        swal({
                            title: "Crear",
                            text: "Registro creado correctamente",
                            icon: "success"
                        }).then((value) => {
                          location.href = 'http://localhost/repositorios/app/usuarios/usuario.php';
                        });
                    }
                });
            }
    </script>
    <h1 class="text-center">
        Crear Usuario
    </h1>
    <div class="container card">
        <form class="card-body" autocomplete="off" action="#">
            <input type="hidden" id="funcion" value="crea-actualiza">
            <div class="mb-3">
                <label class="form-label">Nombre<req>*</req></label>
                <input type="text" class="form-control" id="nombre" aria-describedby="emailHelp" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Correo<req>*</req></label>
                <input type="email" class="form-control" id="correo" aria-describedby="emailHelp" required>
            </div>
            <div class="mb-3">
                <label class="form-label">contraseña<req>*</req></label>
                <input type="password" class="form-control" id="contrasena" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Perfil<req>*</req></label>
                <select class="form-select form-select" id="cod_perfil" aria-label=".form-select-sm example" required>
                    <option value="">Seleccione...</option>
                    {foreach $perfiles as $perfil}
                        <option value="{$perfil.cod_perfil}">{$perfil.nombre}</option>
                    {/foreach}
                </select>
            </div>
            <div class="d-grid gap-2 col-6 mx-auto">
                <button type="button" class="btn btn-primary" onclick="crear()">Crear</button>
            </div>
        </form>
    </div>
{/block}