{extends file = "index.tpl"}
{block name=title}Repositorios{/block}
{block name=body}
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><strong>Recurso: </strong> {$repositorio.nombre}</h5>
                <br>
                <h6 class="card-subtitle mb-2"> <strong> Usuario: </strong> {$repositorio.usuario}</h6>
                <p class="card-text"> <strong> Contenido:  </strong> {$repositorio.contenido}</p>
                <a><strong> Fecha: </strong> {$repositorio.fecha} </a>
            </div>
        </div>
    </div>

{/block}