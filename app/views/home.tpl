{extends file = "index.tpl"}
{block name=title}Inicio{/block}
{block name=body}
    <script>
        const usuario = '{$smarty.cookies.cod_usuario}'
        const perfil = '{$smarty.cookies.perfil}'
        localStorage.setItem('usuario', usuario);
        localStorage.setItem('perfil', perfil);
    </script>
    <h3 class="text-center">
        Bienvenido, <strong>{$nombre_usuario}</strong>
    </h3>
    <div class="row justify-content-md-center">
        <input type="hidden" id="perfil" value="">
        {if 'repositorios'|in_array:$permisos}
            <div class="card col-md-3" style="padding: 0%; margin: 1%;">
                <img src="https://images.adsttc.com/media/images/57a8/08d0/e58e/ce75/2000/0114/large_jpg/%E5%B1%8F%E5%B9%95%E5%BF%AB%E7%85%A7_2016-08-08_%E4%B8%8A%E5%8D%8811.55.54.jpg?1470630092"
                     class="card-img-top" alt="Recursos">
                <div class="card-body">
                    <h5 class="card-title">
                        <strong>Recursos</strong>
                    </h5>
                    <p class="card-text">
                        Aqui encontraras el listado de recursos disponibles.
                    </p>
                    <div class="text-center">
                        <a href="/repositorios/app/repositorios/repositorio.php" class="btn btn-primary">Llevame
                            alla!</a>
                    </div>
                </div>
            </div>
        {/if}
        {if 'historial'|in_array:$permisos}
            <div class="card col-md-3" style="padding: 0%; margin: 1%;">
                <img src="https://miro.medium.com/max/1000/1*Em0y6wGmfuWIFUEQYgil7g.png" class="card-img-top"
                     alt="Historial" style="height: 255px">
                <div class="card-body">
                    <h5 class="card-title">
                        <strong>Historial Recursos</strong>
                    </h5>
                    <p class="card-text">
                        Aqui encontraras el historial de los recursos disponibles, junto el responsable y la fecha
                        en la
                        que
                        se realizo la acción.
                    </p>
                    <div class="text-center">
                        <a href="/repositorios/app/repositorios/repositorio.php" class="btn btn-primary">Llevame
                            alla!</a>
                    </div>
                </div>
            </div>
        {/if}
        {if 'usuarios'|in_array:$permisos}
            <div class="card col-md-3" style="padding: 0%; margin: 1%;">
                <img src="https://dpbnri2zg3lc2.cloudfront.net/en/wp-content/uploads/old-blog-uploads/what-are-personas-fictitious-users-example.jpg"
                     class="card-img-top" alt="Usuarios" style="height: 255px">
                <div class="card-body">
                    <h5 class="card-title">
                        <strong>
                            Usuarios
                        </strong>
                    </h5>
                    <p class="card-text">
                        Aqui encontraras el listado de usuarios registrados en el sistema.
                    </p>
                    <div class="text-center">
                        <a href="/repositorios/app/usuarios/usuario.php" class="btn btn-primary">Llevame alla!</a>
                    </div>
                </div>
            </div>
        {/if}
    </div>
{/block}