<!doctype html>
<html lang="es">
<head>
    {block name=head}
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    {/block}
    <style>
        body {
            padding: 0 !important;
            font-family: 'Nunito', sans-serif;
        }
        req {
            color: red;
        }
    </style>
    <title>Ingreso</title>
</head>
<body class="container-fluid">
<div class="row text-center" style="background-color: black; color: #ffc107">
    <div class="row">&nbsp;</div>
    <h2>
        <i class="fas fa-book-reader"></i> Recursos
    </h2>
    <div class="row">&nbsp;</div>
</div>
<h1 class="text-center" style="margin-top: 5%">Ingreso</h1>
<div class="row justify-content-md-center">
    <div class="card col-md-6" style="padding: 0%">
        {if $credenciales_incorrectas}
            <div class="alert alert-danger">
                Credenciales incorrectas.
            </div>
        {/if}
        <img src="https://www.xtrafondos.com/wallpapers/diseno-de-amarillo-negro-y-gris-5820.jpg"
             class="card-img-top" alt="Repositorios" style="height: 200px">
        <div class="card-body">
            <form class="form" method="post" action="login.php">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><strong>Email<req>*</req></strong></label>
                        <input class="form-control" type="email" name="email" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label><strong>Contraseña<req>*</req></strong></label>
                        <input class="form-control" type="password" name="contrasena" required>
                    </div>
                </div>
                <br>
                <div class="form-group col-md-12 text-center">
                    <button class="btn btn-warning" type="submit">
                        <strong style="color: white">Ingresar</strong>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>