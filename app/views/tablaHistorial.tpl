{extends file = "index.tpl"}
{block name=title}Historial{/block}
{block name=body}
    <h1 class="text-center">
        Historial Recursos
    </h1>
    <div class="container card">
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Recurso</th>
                    <th>Usuario</th>
                    <th>Accion</th>
                    <th>Fecha</th>
                    <th>Contenido</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=repositorio from=$historial}
                    <tr>
                        <td>{$repositorio.nombre}</td>
                        <td>{$repositorio.usuario}</td>
                        <td>{$repositorio.accion}</td>
                        <td>{$repositorio.fecha}</td>
                        <td>{$repositorio.contenido}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
{/block}