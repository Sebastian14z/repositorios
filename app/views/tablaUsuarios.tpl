{extends file = "index.tpl"}
{block name=title}Usuarios{/block}
{block name=body}
    <script>
        $(document).ready();

        function eliminar(id_usuario) {
            console.log(id_usuario);
            $.ajax({
                url: "/repositorios/app/usuarios/usuario.php?funcion=eliminar&usuario=" + id_usuario,
                success: function (result) {
                    swal({
                        title: "Eliminar",
                        text: "Registro eliminado correctamente",
                        icon: "info"
                    }).then((value) => {
                        location.reload();
                    });
                }
            });
        }
    </script>
    <h1 class="text-center">
        Usuarios
    </h1>
    <div class="container card">
        <div class="card-body">
            <div class="col-md-2">&nbsp;</div>
            <div class="row">
                <div class="row">
                    <div class="col-md-11">&nbsp;</div>
                    <a type="button" class="btn btn-success col-md-1"
                       href="/repositorios/app/usuarios/usuario.php?funcion=crear-actualiza"><i
                                class="fas fa-plus"></i></a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Perfil</th>
                        <th scope="col">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=usuario from=$usuarios}
                        <tr>
                            <th>{$usuario.nombre}</th>
                            <td>{$usuario.correo}</td>
                            <td>{$usuario.perfil}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a type="button" class="btn btn-warning" data-usuario="{$usuario.cod_usuario}"
                                       href="/repositorios/app/usuarios/usuario.php?funcion=crear-actualiza&usuario={$usuario.cod_usuario}">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                    <button type="button" class="btn btn-danger eliminar"
                                            onclick="eliminar({$usuario.cod_usuario})">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
{/block}