{extends file = "index.tpl"}
{block name=title}Editar usuario{/block}
{block name=body}
    <script>
            function actualizar() {
                let form = {
                    funcion: $('#funcion').val(),
                    cod_usuario: $('#cod_usuario').val(),
                    nombre: $('#nombre').val(),
                    correo: $('#correo').val(),
                    contrasena: $('#contrasena').val(),
                    cod_perfil: $('#cod_perfil').val()
                }
                $.ajax({
                    url: "/repositorios/app/usuarios/usuario.php?funcion=crea-actualiza&usuario=" + form.cod_usuario,
                    method: 'post',
                    data: form,
                    success: function(result) {
                        swal({
                            title: "Editar",
                            text: "Registro editado correctamente",
                            icon: "success"
                        }).then((value) => {
                           location.href = 'http://localhost/repositorios/app/usuarios/usuario.php';
                        });
                    }
                });
            }
    </script>
    <h1 class="text-center">
        Editar Usuario
    </h1>
    <div class="container card">
        <form class="card-body" autocomplete="off" action="#" id="form_usuario">
            <input type="hidden" id="funcion" value="crea-actualiza">
            <input type="hidden" id="cod_usuario" value="{$usuario.cod_usuario}">
            <div class="mb-3">
                <label class="form-label">Nombre<req>*</req></label>
                <input type="text" class="form-control" id="nombre" aria-describedby="emailHelp" value="{$usuario.nombre}"
                    required>
            </div>
            <div class="mb-3">
                <label class="form-label">Correo<req>*</req></label>
                <input type="email" class="form-control" id="correo" aria-describedby="emailHelp" value="{$usuario.correo}"
                    required>
            </div>
            <div class="mb-3">
                <label class="form-label">contraseña<req>*</req></label>
                <input type="password" class="form-control" id="contrasena" value="{$usuario.contrasena}" required>
            </div>
            <div class="mb-3">
                <label class="form-label">Perfil<req>*</req></label>
                <select class="form-select form-select" id="cod_perfil" aria-label=".form-select-sm example" required>
                    <option value="">Seleccione...</option>
                    {foreach $perfiles as $perfil}
                        <option value="{$perfil.cod_perfil}" {if $perfil.cod_perfil eq $usuario.cod_perfil}selected{/if}>
                            {$perfil.nombre}</option>
                    {/foreach}
                </select>
            </div>
            <div class="d-grid gap-2 col-6 mx-auto">
                <button type="button" class="btn btn-primary" onclick="actualizar()">Editar</button>
            </div>
        </form>
    </div>
{/block}