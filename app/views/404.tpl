{extends file = "index.tpl"}
{block name=title}404{/block}
{block name=body}
    <style>
        #img404 {
            width: 100%;  /*width of parent container*/
            height: 100%; /*height of parent container*/
            object-fit: contain;
            position: relative;
            aspect-ratio: auto;
            top: 50%;
        }
    </style>
    <img id="img404" src=".\public\images\404.jpg" alt="404">
{/block}