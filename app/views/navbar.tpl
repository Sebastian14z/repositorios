{if $habilita_navbar}
    <script>
        function test() {
            var tabsNewAnim = jQuery('#navbarSupportedContent');
            var selectorNewAnim = jQuery('#navbarSupportedContent').find('li').length;
            var activeItemNewAnim = tabsNewAnim.find('.active');
            var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
            var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
            var itemPosNewAnimTop = activeItemNewAnim.position();
            var itemPosNewAnimLeft = activeItemNewAnim.position();
            jQuery(".hori-selector").css({
                "top": itemPosNewAnimTop.top + "px",
                "left": itemPosNewAnimLeft.left + "px",
                "height": activeWidthNewAnimHeight + "px",
                "width": activeWidthNewAnimWidth + "px"
            });
            jQuery("#navbarSupportedContent").on("click", "li", function (e) {
                jQuery('#navbarSupportedContent ul li').removeClass("active");
                jQuery(this).addClass('active');
                var activeWidthNewAnimHeight = jQuery(this).innerHeight();
                var activeWidthNewAnimWidth = jQuery(this).innerWidth();
                var itemPosNewAnimTop = jQuery(this).position();
                var itemPosNewAnimLeft = jQuery(this).position();
                jQuery(".hori-selector").css({
                    "top": itemPosNewAnimTop.top + "px",
                    "left": itemPosNewAnimLeft.left + "px",
                    "height": activeWidthNewAnimHeight + "px",
                    "width": activeWidthNewAnimWidth + "px"
                });
            });
        }

        jQuery(document).ready(function () {
            setTimeout(function () {
                test();
            });
        });
        jQuery(window).on('resize', function () {
            setTimeout(function () {
                test();
            }, 500);
        });
        jQuery(".navbar-toggler").click(function () {
            jQuery(".navbar-collapse").slideToggle(300);
            setTimeout(function () {
                test();
            });
        });

        // --------------add active class-on another-page move----------
        jQuery(document).ready(function (jQuery) {
            // Get current path and find target link
            var path = window.location.pathname.split("/").pop();

            // Account for home page with empty path
            if (path == '') {
                path = 'index.html';
            }

            var target = jQuery('#navbarSupportedContent ul li a[href="' + path + '"]');
            // Add active class to target link
            target.parent().addClass('active');
        });

        function cerrarSesion() {
            $.ajax({
                url: "/repositorios/app/login.php?funcion=cerrar-sesion",
                success: function () {
                    location.href = '/repositorios/app/login.php';
                }
            });
        }
    </script>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto');

        body {
            font-family: 'Roboto', sans-serif;
        }

        * {
            margin: 0;
            padding: 0;
        }

        i {
            margin-right: 10px;
        }

        /*----------bootstrap-navbar-css------------*/
        .navbar-logo {
            padding: 15px;
            color: #fff;
        }

        .navbar-mainbg {
            background-color: #DB9E23;
            padding: 0px;
        }

        #navbarSupportedContent {
            overflow: hidden;
            position: relative;
        }

        #navbarSupportedContent ul {
            padding: 0px;
            margin: 0px;
        }

        #navbarSupportedContent ul li a i {
            margin-right: 10px;
        }

        #navbarSupportedContent li {
            list-style-type: none;
            float: left;
        }

        #navbarSupportedContent ul li a {
            color: rgba(255, 255, 255, 0.5);
            text-decoration: none;
            font-size: 15px;
            display: block;
            padding: 20px 20px;
            transition-duration: 0.6s;
            transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
            position: relative;
        }

        #navbarSupportedContent > ul > li.active > a {
            color: black;
            background-color: transparent;
            transition: all 0.7s;
        }

        #navbarSupportedContent a:not(:only-child):after {
            content: "\f105";
            position: absolute;
            right: 20px;
            top: 10px;
            font-size: 14px;
            font-family: "Font Awesome 5 Free";
            display: inline-block;
            padding-right: 3px;
            vertical-align: middle;
            font-weight: 900;
            transition: 0.5s;
        }

        #navbarSupportedContent .active > a:not(:only-child):after {
            transform: rotate(90deg);
        }

        .hori-selector {
            display: inline-block;
            position: absolute;
            height: 100%;
            top: 0px;
            left: 0px;
            transition-duration: 0.6s;
            transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
            background-color: #fff;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
            margin-top: 10px;
        }

        .hori-selector .right,
        .hori-selector .left {
            position: absolute;
            width: 25px;
            height: 25px;
            background-color: #fff;
            bottom: 10px;
        }

        .hori-selector .right {
            right: -25px;
        }

        .hori-selector .left {
            left: -25px;
        }

        .hori-selector .right:before,
        .hori-selector .left:before {
            content: '';
            position: absolute;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            background-color: #DB9E23;
        }

        .hori-selector .right:before {
            bottom: 0;
            right: -25px;
        }

        .hori-selector .left:before {
            bottom: 0;
            left: -25px;
        }


        @media (min-width: 992px) {
            .navbar-expand-custom {
                -ms-flex-flow: row nowrap;
                flex-flow: row nowrap;
                -ms-flex-pack: start;
                justify-content: flex-start;
            }

            .navbar-expand-custom .navbar-nav {
                -ms-flex-direction: row;
                flex-direction: row;
            }

            .navbar-expand-custom .navbar-toggler {
                display: none;
            }

            .navbar-expand-custom .navbar-collapse {
                display: -ms-flexbox !important;
                display: flex !important;
                -ms-flex-preferred-size: auto;
                flex-basis: auto;
            }
        }


        @media (max-width: 991px) {
            #navbarSupportedContent ul li a {
                padding: 12px 30px;
            }

            .hori-selector {
                margin-top: 0px;
                margin-left: 10px;
                border-radius: 0;
                border-top-left-radius: 25px;
                border-bottom-left-radius: 25px;
            }

            .hori-selector .left,
            .hori-selector .right {
                right: 10px;
            }

            .hori-selector .left {
                top: -25px;
                left: auto;
            }

            .hori-selector .right {
                bottom: -25px;
            }

            .hori-selector .left:before {
                left: -25px;
                top: -25px;
            }

            .hori-selector .right:before {
                bottom: -25px;
                left: -25px;
            }
        }
    </style>
    <nav class="navbar navbar-expand-custom navbar-mainbg" style="margin-bottom: 5%">
        <a class="navbar-brand navbar-logo" href="/repositorios/app/home.php">
            <i class="fas fa-book-reader"></i> Recursos
        </a>
        <button class="navbar-toggler" type="button" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <i class="fas fa-bars text-white"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto col-md-10">
                <div class="hori-selector">
                    <div class="left"></div>
                    <div class="right"></div>
                </div>
                {if 'home'|in_array:$permisos}
                    <li class="nav-item  {if 'home' eq $tab_activa}active{/if}">
                        <a class="nav-link" href="/repositorios/app/home.php">
                            <i class="fas fa-house-user"></i>Inicio
                        </a>
                    </li>
                {/if}
                {if 'repositorios'|in_array:$permisos}
                    <li class="nav-item {if 'repositorios' eq $tab_activa}active{/if}">
                        <a class="nav-link" href="/repositorios/app/repositorios/repositorio.php">
                            <i class="fas fa-book"></i>Recursos
                        </a>
                    </li>
                {/if}
                {if 'historial'|in_array:$permisos}
                    <li class="nav-item {if 'historial' eq $tab_activa}active{/if}">
                        <a class="nav-link"
                           href="/repositorios/app/repositorios/historial-repositorio.php">
                            <i class="far fa-address-book"></i>Historial
                        </a>
                    </li>
                {/if}
                {if 'usuarios'|in_array:$permisos}
                    <li class="nav-item  {if 'usuarios' eq $tab_activa}active{/if}">
                        <a class="nav-link" href="/repositorios/app/usuarios/usuario.php">
                            <i class="fas fa-user"></i>Usuarios
                        </a>
                    </li>
                {/if}
            </ul>
            <div class="col-md-2 text-center">
                {if 'home' eq $tab_activa}
                    <div class="col-md-6" style="color: white">
                        <strong>{$perfil}</strong>
                    </div>
                {/if}
                <div class="col-md-6">
                    <button type="button" class="btn btn-danger" onclick="cerrarSesion()"
                            style="font-size: {if 'home' eq $tab_activa}10px{else}13px{/if}">
                        <i class="fas fa-sign-out-alt"></i> Cerrar sesion
                    </button>
                </div>
            </div>
        </div>
    </nav>
{/if}