{extends file = "index.tpl"}
{block name=title}Repositorios{/block}
{block name=body}
    <script>

        $(document).ready(function () {
            const perfil = localStorage.getItem('perfil');
            if (perfil === 'Estudiante') {
                $('.admin-options').hide()
            }
        });


        function eliminar(id_repositorio) {
            $.ajax({
                url: "/repositorios/app/repositorios/repositorio.php?funcion=eliminar&repositorio=" +
                    id_repositorio,
                success: function (result) {
                    swal({
                        title: "Eliminar",
                        text: "Recurso eliminado correctamente",
                        icon: "info"
                    }).then((value) => {
                        location.reload();
                    });
                }
            });
        }
    </script>
    <h1 class="text-center">
        Recursos
    </h1>
    <div class="container card">
        <div class="card-body">
            <div class="col-md-2">&nbsp;</div>
            <div class="row">
                <div class="row">
                    <div class="col-md-11">&nbsp;</div>
                    <a type="button" class="btn btn-success col-md-1"
                       href="/repositorios/app/repositorios/repositorio.php?funcion=crear-actualiza"><i
                                class="fas fa-plus"></i></a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Usuario</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=repositorio from=$repositorios}
                        <tr>
                            <td>{$repositorio.nombre}</td>
                            <td>{$repositorio.usuario}</td>
                            <td>{$repositorio.fecha}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a type="button" class="btn btn-info"
                                       href="/repositorios/app/repositorios/repositorio.php?funcion=consultar&repositorio={$repositorio.cod_repositorio}"
                                       target="_blank">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <div class="admin-options">
                                        <a type="button" class="btn btn-warning"
                                           href="/repositorios/app/repositorios/repositorio.php?funcion=crear-actualiza&repositorio={$repositorio.cod_repositorio}">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger eliminar"
                                                onclick="eliminar({$repositorio.cod_repositorio})">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
{/block}