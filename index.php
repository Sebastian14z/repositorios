<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: POST, DELETE, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

ini_set('display_errors', 0);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Bogota');

$url = "http://localhost/repositorios/app/home.php";
header("Location:$url");